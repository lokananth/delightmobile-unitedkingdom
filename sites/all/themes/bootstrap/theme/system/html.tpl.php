<?php
/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see bootstrap_preprocess_html()
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces;?>>
<head profile="<?php print $grddl_profile; ?>">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/icon" href="/sites/all/themes/bootstrap/favicon.ico" sizes="32x32" />
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <?php print $scripts; ?>
  <script src="/sites/all/themes/bootstrap/js/jquery.cookie.js"></script>
  
    <script>
	$(function() {
      $('.footable').footable();
    });

	jQuery(document).ready(function() { 
	
	$('#edit-btn-submit').click( function (){
	if ($('#edit-condtion').is(':checked')) {
var username = $('#edit-username').val();
var password = $('#edit-password').val();
// set cookies to expire in 14 days
$.cookie('username', username, { expires: 14 });
$.cookie('password', password, { expires: 14 });
$.cookie('remember', true, { expires: 14 });
} else {
// reset cookies
$.cookie('username', null);
$.cookie('password', null);
$.cookie('remember', null);
}
});

var remember = $.cookie('remember');
if ( remember == 'true' ) {
var username = $.cookie('username');
var password = $.cookie('password');


// autofill the fields
$('#edit-condtion').attr("checked" , true );
$('#edit-username').val(username);
$('#edit-password').val(password);
}

  });
  
  
  
  
jQuery(document).ready(function($){
$('.field-name-field-terms-and-conditions .field-label').addClass('collapsed').attr('data-toggle','collapse');
$('.field-name-field-terms-and-conditions .field-label').attr('data-target','.field-name-field-terms-and-conditions .field-item');
$('.field-name-field-terms-and-conditions .field-item').addClass('collapse');
$('.webform-confirmation').parent('section').addClass('submit-message');

});



  
   $(document).ready(function () {
   
   if ($(window).width() < 760) {
   //alert('Less than 768');
   $('#block-menu-menu-dm-footer-menu-bundles h2,#block-menu-menu-dm-footer-menu h2,#block-menu-menu-dm-footer-offers h2,#block-menu-menu-dm-footer-support-menu h2').attr('data-toggle','collapse');
   $('#block-menu-menu-dm-footer-menu h2').attr('data-target','#block-menu-menu-dm-footer-menu ul');
   $('#block-menu-menu-dm-footer-menu-bundles h2').attr('data-target','#block-menu-menu-dm-footer-menu-bundles ul');
   $('#block-menu-menu-dm-footer-offers h2').attr('data-target','#block-menu-menu-dm-footer-offers ul');
   $('#block-menu-menu-dm-footer-support-menu h2').attr('data-target','#block-menu-menu-dm-footer-support-menu ul');
   $('#block-menu-menu-dm-footer-menu-bundles ul, #block-menu-menu-dm-footer-menu ul,#block-menu-menu-dm-footer-offers ul,#block-menu-menu-dm-footer-support-menu ul').addClass('collapse');
}
else {
   //alert('More than 768');
}
    
}); 
	 


	$(document).ready(function () {


    $('input.disablecopypaste').bind('copy paste', function (e) {
       e.preventDefault();
    });
                //$(".disablecopypaste").numeric();

                 $(".disablecopypaste,.disable_mnumber").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
                
    
	$("#edit-card-number").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
   
  
	 $('#edit-cardholder-name').keydown(function (e) {
       if (e.ctrlKey || e.altKey) {
          e.preventDefault();
        } else {
          var key = e.keyCode;
          if (!((key == 8 || key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
          }
        }
    });
	
	$("#edit-security-code").bind("keydown", function (event) {
       if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
              // let it happen, don't do anything
              return;
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
   });


  });
  
  
   $(document).ready(function ($) {
	 
	$("#edit-mobile-number,#edit-username,#edit-verification-code,#edit-contact-number").keypress(function (e) {
		 //if the letter is not digit then display error and don't type anything
		 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			$("#errmsg").html("Digits Only").show().fadeOut("slow");
				   return false;
		}
   });
   
   
    $("#vcode").on( 'click' ,function(e){
            var mobileno='<?php echo $_SESSION['Reg_mobileno'];?>';
           $.ajax({
              url: '/myaccount/register/pin_resend',
              type: "POST",
              success: function(data){
                 //alert("New OTP PIN number"+data);
             //  $('#pin_code').html(data);
			  $(".modal-body p").text( "New OTP PIN number" + data );
			   $("#myModal").modal();
              }
          });
      });

	  

/*   $(",#edit-contact-number").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
 */
  $('#edit-first-name,#edit-last-name').bind('keypress keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-z A-Z]/g,'') ); 
	});
  
 
  
 $('#edit-email,#edit-confirm-email').bind('keypress keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-zA-Z0-9@._-]/g,'')); 
	
	});
 
    $('#edit-password,#edit-confirm-password').keypress(function(key) {
        if(key.charCode == 32) return false;
    });
	
	$('#edit-password,#edit-confirm-password,#edit-confirm-email').bind("paste",function(e) {
          e.preventDefault();
      });
 
	  $('.visa-c').on('click', function(){
	  
	    window.open('http://www.mastercard.co.uk/securecode.html','mywindowtitle','width=700,height=650');
	 
	  }); 
	  $('.master-c').on('click', function(){
	  
	    window.open('http://www.visaeurope.com/en/cardholders/verified_by_visa.aspx','mywindowtitle','width=700,height=650');
	 
	  });
   }); 
  
</script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
<script>
$.fn.scrollView = function () {
  return this.each(function () {
  
    $('html, body').animate({
      scrollTop: $(this).parent().offset().top
    }, 'slow' );

  });
}
$(function(){

if ( $('#main-content').next().is('.alert-block')) {
	$('.alert-block').scrollView();
}

else  {
	$('#topup-registration-form, #myaccount-login-form, #myaccount-registration-form, #myaccount-forgot-password-form, #freesim-registration-form' ).parent().parent().scrollView();
}





});
</script>
</html>
