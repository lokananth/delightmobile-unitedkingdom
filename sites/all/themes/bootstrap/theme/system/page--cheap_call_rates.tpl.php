<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
 global $base_url;
?>
<?php if (!empty($page['language_switcher'])): ?>
		<div id="language_switcher">
		    <div class="container">
				<?php print render($page['language_switcher']); ?> 
			</div>	
		</div>		
	<?php endif; ?>
	
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
 
    <div class="navbar-header">
      <?php if ($logo): ?>
       <a class="logo navbar-btn pull-right" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
	   <!-- <a class="offer-logo pull-left" href="" title="">
        <img src="sites/all/themes/bootstrap/offer-logo.png" alt="<?php //print t('Home'); ?>" />
      </a> -->
      <?php endif; ?>

      <?php if (!empty($site_name)): ?>
      <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>

      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
      </div>
    <?php endif; ?>
 
</header>

<div class="container title-banner">
<div id="dm-title">
	<?php

	// banner title has been diplayed on the page explicityly
	if(isset($node->field_banner_title['und'][0]['value'])) {  echo "<h1 class='yellow'>".$node->field_banner_title['und'][0]['value']."</h1>"; }
	
	if(isset($node->field_banner_title['und'][1]['value'])) {  echo "<h1 class='white'>".$node->field_banner_title['und'][1]['value']."</h1>"; }
	?>
	
<div id="main_country_flag" class="pull-right">
	<?php 
		$main_country_flag = substr($_SERVER['REQUEST_URI'],13);
		if(isset($main_country_flag)) { echo '<img src="sites/default/files/pictures/country_flags/main_country_flag/'.$main_country_flag.'.png" />'; } 
		
	?>
</div>
	
	
	
</div>
</div>
<?php 
	
	if(isset($node->field_header_banner['und'][0]['is_default']) && ($node->field_header_banner['und'][0]['is_default']==1))
	{
		if(isset($node->field_header_banner['und'][0]['filename'])) { echo '<div id="header_banner"><img src="sites/default/files/pictures/default_images/'.$node->field_header_banner['und'][0]['filename'].'" /></div>'; } 
	}
	else
	{	
	if(isset($node->field_header_banner['und'][0]['filename'])) { echo '<div id="header_banner"><img src="sites/default/files/pictures/'.$node->field_header_banner['und'][0]['filename'].'" /></div>'; } 
	} 
?>


<div class="main-container container">

  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->
 
  
  <div class="row">
  
	<!-- /#sidebar-first -->
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  
    <?php endif; ?>

    <section id="cheap-callrates" class="col-sm-12 clearfix">
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php //if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php //if (!empty($title)): ?>
       <!-- <h1 class="page-header"><?php //print $title; ?></h1>-->
      <?php //endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
	  
	 
	
	 <div class="col-sm-7">
	  <div class="cheap_call_rates_title">
	 <?php

	// page title has been diplayed on the page explicityly
	if(isset($node->field_page_title['und'][0]['value'])) {  echo "<h1>".$node->field_page_title['und'][0]['value']."</h1>"; }
	?>
	
	 </div>
     <?php print render($page['content']); ?>
	 <div id="cheap-call-rates-section">

		<?php $node->title; ?>
		
		<?php if (!empty($page['delight_cheapcallrates_section_block'])): ?>
		<div class="rate-table row">
         <?php print render($page['delight_cheapcallrates_section_block']); ?>
		</div> 
		<?php endif; ?>
	
	
		
		
        
		
		<div class="cheap_call_rates_body_content2">
			 <?php

			// page title has been diplayed on the page explicitly
			if(isset($node->field_body_content_part2['und'][0]['value'])) {  echo "<p>".$node->field_body_content_part2['und'][0]['value']."</p>"; }
			?>
			
			 </div>
		<?php if (!empty($page['delight_supporting_country_section_block'])): ?>
				<?php print render($page['delight_supporting_country_section_block']); ?> 
		<?php endif; ?>

		</div> 
	 </div>
	 
	 <div class="col-sm-5">
	 
	 
	   <form action="/order/freesim" method="post" id="freesim-registration-form" accept-charset="UTF-8">
	   <div>
		<div class="free-sim"><div id="edit-form-title" class="form-type-item form-item form-group">
		  <label for="edit-form-title">Choose your SIM Type </label>
		 
		</div>
		<div id="edit-sim-type-img" class="form-type-item form-item form-group">
		 <span><img src="../sites/default/files/pictures/standard-sim.png"></span> <span><img src="../sites/default/files/pictures/nano-sim.png"></span>
		</div>
		<div class="form-type-radios form-item-sim-type form-item form-group" data-toggle="tooltip" title="Choose your SIM Type to register to get the free sim.">
		  <label for="edit-sim-type">Choose your SIM Type </label>
		 <div id="edit-sim-type" class="form-radios"><div class="form-type-radio form-item-sim-type form-item radio">
		 <input type="radio" id="edit-sim-type-0" name="sim-type" value="0" checked="checked" class="form-radio" />  <label for="edit-sim-type-0">Standard</br> Micro SIM </label>

		</div>
		<div class="form-type-radio form-item-sim-type form-item radio">
		 <input type="radio" id="edit-sim-type-1" name="sim-type" value="1" class="form-radio" />  <label for="edit-sim-type-1">Nano SIM </label>

		</div>
		</div>
		</div>
		<div id="edit-personal-details" class="form-type-item form-item form-group">
		  <label for="edit-personal-details">Personal details </label>
		 
		</div>
		<div class="form-type-select form-item-reg-title form-item form-group">
		  <label for="edit-reg-title">Title <span class="form-required" title="This field is required.">*</span></label>
		 <select class="form-control form-select required" id="edit-reg-title" name="reg_title"><option value="" selected="selected">- Select -</option><option value="Mr">Mr</option><option value="Mrs">Mrs</option><option value="Ms">Ms</option><option value="Dr">Dr</option><option value="Prof">Prof</option><option value="Sir">Sir</option><option value="Lady">Lady</option><option value="Rev">Rev</option><option value="Miss">Miss</option><option value="Lord">Lord</option><option value="Other">Other</option></select>
		</div>
		<div class="form-type-textfield form-item-first-name form-item form-group" data-toggle="tooltip" title="Please enter the first name">
		  <label for="edit-first-name">First Name <span class="form-required" title="This field is required.">*</span></label>
		 <input class="form-control form-text required" type="text" id="edit-first-name" name="first_name" value="" size="60" maxlength="25" />
		</div>
		<div class="form-type-textfield form-item-last-name form-item form-group" data-toggle="tooltip" title="Please enter the last name">
		  <label for="edit-last-name">Last Name <span class="form-required" title="This field is required.">*</span></label>
		 <input class="form-control form-text required" type="text" id="edit-last-name" name="last_name" value="" size="60" maxlength="25" />
		</div>
		<div class="form-type-textfield form-item-contact-number form-item form-group" data-toggle="tooltip" title="Please enter the contact number">
		  <label for="edit-contact-number">Contact Number <span class="form-required" title="This field is required.">*</span></label>
		 <input class="form-control form-text required" type="text" id="edit-contact-number" name="contact_number" value="" size="60" maxlength="15" />
		</div>
		<div class="form-type-textfield form-item-email form-item form-group">
		  <label for="edit-email">Email Address <span class="form-required" title="This field is required.">*</span></label>
		 <input class="form-control form-text required" type="text" id="edit-email" name="email" value="" size="60" maxlength="128" />
		</div>
		<div id="edit-delivery-address" class="form-type-item form-item form-group">
		  <label for="edit-delivery-address">Delivery Address </label>
		 
		</div>
		<div id="edit-delivery-address1" class="form-type-item form-item form-group">
  <label for="edit-delivery-address1">Postal Code </label>
 <div  id='postcode_lookup'></div>
</div>
<input id="opc_input" type="hidden" name="pin_code" value="" />
		
		<div class="form-type-textfield form-item-address1 form-item form-group" data-toggle="tooltip" title="Please enter the address1">
		  <label for="edit-address1"><br>Address Line1 <span class="form-required" title="This field is required.">*</span></label>
		 <input readonly="readonly" class="form-control form-text required" type="text" id="edit-address1" name="address1" value="" size="60" maxlength="50" />
		</div>
		<div class="form-type-textfield form-item-address2 form-item form-group" data-toggle="tooltip" title="Please enter the address2">
		  <label for="edit-address2">Address Line2</label>
		 <input readonly="readonly" class="form-control form-text" type="text" id="edit-address2" name="address2" value="" size="60" maxlength="50" />
		</div>
		<div class="form-type-textfield form-item-address3 form-item form-group" data-toggle="tooltip" title="Please enter the address3">
		  <label for="edit-address3">Address Line3 <span class="form-required" title="This field is required.">*</span></label>
		 <input readonly="readonly" class="form-control form-text required" type="text" id="edit-address3" name="address3" value="" size="60" maxlength="50" />
		</div>
		<div class="form-type-textfield form-item-town form-item form-group">
		  <label for="edit-town">Town <span class="form-required" title="This field is required.">*</span></label>
		 <input readonly="readonly" class="form-control form-text required" type="text" id="edit-town" name="town" value="" size="60" maxlength="50" />
		</div>
		<div class="form-type-textfield form-item-country form-item form-group">
		  <label for="edit-country">Country <span class="form-required" title="This field is required.">*</span></label>
		 <input readonly="readonly" class="form-control form-text required" type="text" id="edit-country" name="country" value="United Kingdom" size="60" maxlength="128" />
		</div>
		<div class="form-type-checkbox form-item-condition form-item checkbox">
		 <input type="checkbox" id="edit-condition" readonly="readonly" name="condition" value="1" class="form-checkbox required" />  <label for="edit-condition">By ticking this box, you confirm to accept our Terms &amp; Conditions and our Privacy Policy <span class="form-required" title="This field is required.">*</span></label>

		</div>
		<button class="btn btn-default form-submit" id="edit-btn-submit" name="op" value="I want it now" type="submit">I want it now</button>
		</div>
		<input type="hidden" name="form_build_id" value="form--Yn9I1FVlVBjsmkCx60mD9Z07sA7nE9SbKwzllCqVZs" />
		<input type="hidden" name="form_token" value="AuTlXTEjPojGNUTwOtrAyGHFayAZPDz92FgMusATPKY" />
		<input type="hidden" name="form_id" value="freesim_registration_form" />
	</div>
</form>
	 
	 
	 </div>
    </section>

	<!-- /#sidebar-second -->
    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  
    <?php endif; ?>

  </div>
</div>



<div id="supporting_country-section">
<div class="sub-header">
<p class="container">Choose your country to make a cheap international calls</p>
</div>
<br>

<?php 

//echo '<pre>';
//print_r($node);

 $scountry1= $node->field_supporting_country['und'][0]['tid'];
 $scountry2= $node->field_supporting_country2['und'][0]['tid'];
 $scountry3= $node->field_supporting_country3['und'][0]['tid'];
 $scountry4= $node->field_supporting_country4['und'][0]['tid'];


$scountry5= $node->field_supporting_country5['und'][0]['tid'];
$scountry6= $node->field_supporting_country6['und'][0]['tid'];
$scountry7= $node->field_supporting_country7['und'][0]['tid'];
$scountry8= $node->field_supporting_country8['und'][0]['tid'];

//print_r($node->field_supporting_country);

$supportingCountry = array("1"=>"Afghanistan",
"2"=>"Algeria",
"3"=>"Australia",
"4"=>"Bangladesh",
"5"=>"Brazil",
"6"=>"Canada",
"7"=>"China",
"8"=>"Egypt",
"9"=>"France",
"10"=>"Ghana",
"11"=>"Hongkong",
"12"=>"Hungary",
"13"=>"India",
"14"=>"Ireland",
"15"=>"Italy",
"16"=>"Japan",
"17"=>"Kenya",
"18"=>"Latvia",
"19"=>"Lithuania",
"20"=>"Malaysia",
"21"=>"Mexico",
"22"=>"Morocco",
"23"=>"Netherlands",
"24"=>"Nigeria",
"25"=>"Pakistan",
"26"=>"Poland",
"27"=>"Romania",
"28"=>"Saudiarabia",
"29"=>"Slovakia",
"30"=>"Srilanka",
"31"=>"Thailand",
"32"=>"Turkey",
"33"=>"Uganda",
"34"=>"UAE",
"35"=>"USA");

$sc1= $supportingCountry[$scountry1];
$sc2= $supportingCountry[$scountry2];
$sc3= $supportingCountry[$scountry3];
$sc4= $supportingCountry[$scountry4];

$sc5= $supportingCountry[$scountry5];
$sc6= $supportingCountry[$scountry6];
$sc7= $supportingCountry[$scountry7];
$sc8= $supportingCountry[$scountry8];



?>



<div class="container"> 
<ul class="list-inline">
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc1;?>.png"><a><?php echo $sc1;?></a></li>
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc2;?>.png"><a><?php echo $sc2;?></a></li>
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc3;?>.png"><a><?php echo $sc3;?></a></li>
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc4;?>.png"><a><?php echo $sc4;?></a></li>
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc5;?>.png"><a><?php echo $sc5;?></a></li>
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc6;?>.png"><a><?php echo $sc6;?></a></li>
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc7;?>.png"><a><?php echo $sc7;?></a></li>
<li><img src="/sites/default/files/pictures/country_flags/supporting_country_flags/<?php echo $sc8;?>.png"><a><?php echo $sc8;?></a></li>
</ul>


<br>
<?php if (!empty($page['delight_supporting_country_section_block'])): ?>
        <?php print render($page['delight_supporting_country_section_block']); ?> 
<?php endif; ?>
</div>
</div>


<div id="seo_freesim_section">
<div class="container">

<?php if (!empty($page['delight_seo_freesim_section_block'])): ?>
        <?php print render($page['delight_seo_freesim_section_block']); ?> 
<?php endif; ?>
</div>
</div>


<div id="vendor-section">
<div class="container">
<?php if (!empty($page['delight_vendor_section_block'])): ?>
        <?php print render($page['delight_vendor_section_block']); ?> 
<?php endif; ?>
</div>
</div>

<footer id="footer" class="footer">

	<div class="container">
		<?php print render($page['footer']); ?>
    </div>
	
	
	
</footer>
<div id="delight_copyright_section">
		<?php if (!empty($page['delight_copyright_section_block'])): ?>
				<?php print render($page['delight_copyright_section_block']); ?> 
		<?php endif; ?>
</div>

