<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
//error_reporting(E_ALL);
include 'sites/all/themes/bootstrap/theme/system/page_rates.php';

 global $base_url;
?>

<?php if (!empty($page['language_switcher'])): ?>
		<div id="language_switcher">
		    <div class="container">
				<?php print render($page['language_switcher']); ?> 
			</div>	
		</div>		
	<?php endif; ?>
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
 
    <div class="navbar-header">
      <?php if ($logo): ?>
      <a class="logo navbar-btn pull-right" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
	   <a class="offer-logo pull-left" href="<?php print $base_url; ?>/delight-price-guarantee" title="">
        <img src="sites/all/themes/bootstrap/offer-logo.png" alt="<?php print t('Home'); ?>" />
      </a>
      <?php endif; ?>

      <?php if (!empty($site_name)): ?>
      <a class="name navbar-brand" href="" title="<?php print t('delight-price-guarantee'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>

      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
      </div>
    <?php endif; ?>
</header>

<div class="container title-banner">
<div id="dm-title">
	<?php

	// banner title has been diplayed on the page explicityly
	if(isset($node->field_banner_title['und'][0]['value'])) {  echo "<h1 class='yellow'>".$node->field_banner_title['und'][0]['value']."</h1>"; }
	
	if(isset($node->field_banner_title['und'][1]['value'])) {  echo "<h1 class='white'>".$node->field_banner_title['und'][1]['value']."</h1>"; }
	?>
</div>
</div>

<?php 
	
	if(isset($node->field_header_banner['und'][0]['is_default']) && ($node->field_header_banner['und'][0]['is_default']==1))
	{
		if(isset($node->field_header_banner['und'][0]['filename'])) { echo '<div id="header_banner"><img src="sites/default/files/pictures/default_images/'.$node->field_header_banner['und'][0]['filename'].'" /><div class="yellow-hr"></div></div>'; } 
	}
	else
	{	
	if(isset($node->field_header_banner['und'][0]['filename'])) { echo '<div id="header_banner"><img src="sites/default/files/pictures/'.$node->field_header_banner['und'][0]['filename'].'" /><div class="yellow-hr"></div></div>'; } 
	} 
?>

<div class="top-links">
   <a class="tp-lnk" href="/topup/step1">Top Up</a>
   <a class="md-lnk" href="/myaccount/login">My Delight</a>
</div>

<div class="section-bonus">
   <?php print render($page['content']); ?>
</div>

<!-- <div class="section-search">
<h1>International Pay as you Go rates to make you Smile.</h1>
	<form action="" method="POST" name="frm_compare_rates" class="search-form">
	<input name="txt_country" class="srh-box" type="text" value="" placeholder="Find the cheapest International rates to:" />&nbsp;
	</form>
</div>  -->
<?php  homepagerates(); ?>

<div id="homepageRate"> </div>
<!-- <div class="section-search-result">
	<div class="header-bar">
		<div class="container">
		<img src="sites/default/files/pictures/flag-icon/bulgaria.png"><p>Bulgaria Rates</p>
		</div>
	</div>

	<div class="container">
	
    <table class="table1" align="center">
      <thead>
        <tr>
          <th></th>
          <th>
            <img src="sites/all/themes/bootstrap/images/logo-small.png" />
          </th>
          <th>Giff Gaff</th>
          <th>Vodafone PAYG</th>
          <th>O2 PAYG</th>
          <th>EE</th>
          <th>Tesco</th>
          <th>Lyca</th>
          <th>Lebara</th>
          <th>Orange</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>Fixed</th>
          <td>1p</td>
          <td>2p</td>
          <td>£1</td>
          <td>£1.50</td>
          <td>£3.50</td>
          <td>40p</td>
          <td>1p</td>
          <td>1p</td>
          <td>£ 1</td>
        </tr>
        <tr>
          <th>Mobile</th>
          <td>3p</td>
          <td>3p</td>
          <td>£1</td>
          <td>£1.50</td>
          <td>£1.50</td>
          <td>40p</td>
          <td>5p</td>
          <td>8p</td>
          <td>£ 1</td>
        </tr>
        <tr>
          <th>SMS</th>
          <td>15p</td>
          <td>8p</td>
          <td>24p</td>
          <td>20p</td>
          <td>25p</td>
          <td>20p</td>
          <td>19p</td>
          <td>19p</td>
          <td>20p</td>
        </tr>
      </tbody>
    </table>
 </div>
</div> -->




</div>

<div class="main-container container">

  <!-- /#page-header --->
  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </header> 

  <div class="row">

	<!-- /#sidebar-first -->
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  
    <?php endif; ?>

    <section <?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php //if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php //if (!empty($title)): ?>
        <!--<h1 class="page-header"><?php //print $title; ?></h1>-->
      <?php //endif; ?>
      <?php //print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      
    </section>

	<!-- #sidebar-second -->
    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  
    <?php endif; ?>

  </div>
</div>

<div id="international_rates_search-section">
	<?php if (!empty($page['international_rates_search'])): ?>
		<?php print render($page['international_rates_search']); ?> 
	<?php endif; ?>
</div>

<div class="free-sim-block">

<div id="delight_home_section_block">
	
		<?php if (!empty($page['delight_home_section_block'])): ?>
			<?php print render($page['delight_home_section_block']); ?> 
		<?php endif; ?>
	
</div>

<div id="new_delight_free_sim_section">
	
		<?php if (!empty($page['new_delight_free_sim_section'])): ?>
			<?php print render($page['new_delight_free_sim_section']); ?> 
		<?php endif; ?>
	
</div>
</div>

<div id="community-section">
	<?php if (!empty($page['delight_community_section_block'])): ?>
		<?php print render($page['delight_community_section_block']); ?>
	<?php endif; ?>
</div>

<div id="vendor-section">
	<div class="container">
		<?php if (!empty($page['delight_vendor_section_block'])): ?>
			<?php print render($page['delight_vendor_section_block']); ?> 
		<?php endif; ?>
	</div>
</div>

<footer id="footer" class="footer">
	<div class="container">
		<?php print render($page['footer']); ?>
    </div>
</footer>

<div id="delight_copyright_section">
	<?php if (!empty($page['delight_copyright_section_block'])): ?>
		<?php print render($page['delight_copyright_section_block']); ?> 
	<?php endif; ?>
</div>

